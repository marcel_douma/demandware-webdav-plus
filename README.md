# Commerce Cloud WebDAV+

## What is this repository for?

Google Chrome extension to transform Commerce Cloud WebDav browser view into an advanced WebDav client. Including features like sort on date, friendly log files view, drag and drop files and directories directly to the server and download them as well.

The logs are automatically sorted upon last modified first.

### How do I set it up?

This extension is build upon Google Chrome.

* Open the Google Chrome extension tab (chrome://extensions) and enable Developer mode (top right)
* Load the extension using the "Load unpacked extension..." button
* Refresh your Commerce Cloud WebDav view (logs, impex, etc)

## Permissions

* Storage: to save your preferences
* Download: to save the downloaded files
* ActiveTab: to enhance the default Chrome webdav view
* Content Scripts: To be able to add extra functionality like a download button