'use strict';

(function () {
    var modalContainer;

    function keepBMSessionAlive() {
        var refreshIntervalId = setInterval(function () {
            $.get('/on/demandware.store/Sites-Site/default/', function (data, status) {
                if (data && data.indexOf('<!-- Template Name: application/Login -->') > -1) {
                    clearInterval(refreshIntervalId);
                }
            });
        }, 300000); // 5 mins * 60 * 1000 (300000)
    }

    function initModalEditor() {
        // Append a button to all textareas
        let allTextAreas = document.getElementsByTagName('textarea');

        for (var idx = 0; idx < allTextAreas.length; idx++) {
            var currentTextArea = allTextAreas[idx];

            // Check if not already linked
            if (!currentTextArea.getAttribute('data-linked-modal-editor')) {
                // If there is no id, create it
                if (!currentTextArea.id) {
                    currentTextArea.id = 'textarea-' + (idx + 1);
                }

                // Add button to enable or disable html editor
                let btnModalEditor = document.createElement('button');
                btnModalEditor.id = 'btn-modal-editor-' + (idx + 1);
                btnModalEditor.className = 'btn-modal-editor';
                btnModalEditor.setAttribute('data-linked-source', currentTextArea.id);
                btnModalEditor.addEventListener('click', function () {
                    showModalEditor(this);
                });

                btnModalEditor.type = 'button';
                btnModalEditor.style.display = 'block';
                btnModalEditor.appendChild(document.createTextNode('Validate JSON'));
                currentTextArea.parentNode.appendChild(btnModalEditor);
                currentTextArea.setAttribute('data-linked-modal-editor', btnModalEditor.id);
            }
        }
    }

    /**
     * Mutattion observer as Custom preferences are loaded in the DOM dynamically
     */
    function initMutationObersver() {
        let mutationObserver = new MutationObserver(function (mutations) {
            let textAreasAdded = false;

            mutations.forEach(function (mutation) {
                if (mutation.target && mutation.target.nodeName == 'TEXTAREA') {
                    textAreasAdded = true;
                }
            });

            if (textAreasAdded) {
                // Reload Modal editors on textareas
                initModalEditor();
            }
        });

        // Starts listening for changes in the root HTML element of the page.
        let observeContainers = document.getElementsByClassName('bm-ui');
        if (observeContainers && observeContainers.length > 0) {
            for (let item of observeContainers) {
                mutationObserver.observe(item, {
                    attributes: true,
                    characterData: false,
                    childList: false,
                    subtree: true,
                    attributeOldValue: false,
                    characterDataOldValue: false
                });
            }
        }
    }

    // Show editor
    function showModalEditor(el) {
        let linkedTextarea = document.getElementById(el.getAttribute('data-linked-source'));
        let btnModalSave = document.getElementById('btn-modal-save');

        // create the editor
        let container = document.createElement('div');
        container.className = 'modal-editor';
        container.id = 'modal-editor';

        // For now, only json type is supported, maybe later html as well
        btnModalSave.setAttribute('data-modal-editor-type', 'json');

        // Load the JSON validator
        var jsonValidatorContainer = container.appendChild(document.createElement('pre'));

        try {
            let jsonContent = JSON.stringify(jsonlint.parse(linkedTextarea.value), null, 4);
            jsonValidatorContainer.innerHTML = syntaxHighlight(jsonContent);
        } catch (ex) {
            jsonValidatorContainer.className = 'error';
            jsonValidatorContainer.innerHTML = ex.message;

            // Add common errors block
            container.innerHTML += '<div class="info"><p class="heading">Common Errors</p><ul><li>Expecting <code>\'STRING\'</code> - You probably have an extra comma at the end of your collection. Something like <code>{ "a": "b", }</code></li><li>Expecting <code>\'STRING\'</code>, <code>\'NUMBER\'</code>, <code>\'NULL\'</code>, <code>\'TRUE\'</code>, <code>\'FALSE\'</code>, <code>\'{\'</code>, <code>\'[\'</code> - You probably have an extra comma at the end of your list. Something like: <code>["a", "b", ]</code></li><li>Enclosing your collection keys in quotes. Proper format for a collection is <code>{ "key": "value" }</code></li><li>Make sure you follow <a href="http://www.json.org/" target="_blank">JSON\'s syntax</a> properly. For example, always use double quotes, always quotify your keys, and remove all callback functions.</li></ul></div>';
        }

        // Add credits block
        container.innerHTML += '<div class="info"><p class="heading">Credits</p><p>Thanks to <a href="https://jsonlint.com/" target="_blank">https://jsonlint.com/</a></p></div>';


        document.getElementById('modal-body').appendChild(container);

        // Set data attribute on save button
        btnModalSave.setAttribute('data-linked-source', linkedTextarea.id);

        // Show the modal
        modalContainer.style.display = 'block';
    }

    function syntaxHighlight(json) {
        json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
            var cls = 'number';
            if (/^"/.test(match)) {
                if (/:$/.test(match)) {
                    cls = 'key';
                } else {
                    cls = 'string';
                }
            } else if (/true|false/.test(match)) {
                cls = 'boolean';
            } else if (/null/.test(match)) {
                cls = 'null';
            }
            return '<span class="' + cls + '">' + match + '</span>';
        });
    }

    function initModal() {
        // The modal
        modalContainer = document.createElement('div');
        modalContainer.className = 'modal';
        modalContainer.id = 'modal-container';

        let modalContent = document.createElement('div');
        modalContent.className = 'modal-content';
        modalContent.id = 'modal-content';

        let modalBody = document.createElement('div');
        modalBody.className = 'modal-body';
        modalBody.id = 'modal-body';

        let modalToolbar = document.createElement('div');
        modalToolbar.className = 'modal-toolbar';
        modalToolbar.id = 'modal-toolbar';

        // Add save and close buttons
        var btnModalSave = document.createElement('button');
        btnModalSave.id = 'btn-modal-save';
        btnModalSave.type = 'button';
        btnModalSave.appendChild(document.createTextNode('Save'));
        btnModalSave.onclick = function (event) {
            saveModal(event.target);
        }

        var btnModalClose = document.createElement('button');
        btnModalClose.type = 'button';
        btnModalClose.appendChild(document.createTextNode('Or cancel'));
        btnModalClose.onclick = function (event) {
            closeModal();
        }

        modalToolbar.appendChild(btnModalSave);
        modalToolbar.appendChild(btnModalClose);

        modalContent.appendChild(modalBody);
        modalContent.appendChild(modalToolbar);

        modalContainer.appendChild(modalContent);

        document.body.appendChild(modalContainer);

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function (event) {
            if (event.target == modalContainer) {
                closeModal();
            }
        }
    }

    function closeModal() {
        modalContainer.style.display = 'none';
        // clear modal content
        var modalContentNode = document.getElementById('modal-body');
        while (modalContentNode.firstChild) {
            modalContentNode.removeChild(modalContentNode.firstChild);
        }
    }

    function saveModal(el) {
        let linkedTextarea = document.getElementById(el.getAttribute('data-linked-source'));

        if (el.getAttribute('data-modal-editor-type') == 'json') {
            if (linkedTextarea.value.length > 0 && linkedTextarea.value.indexOf('{') == 0) {
                try {
                    linkedTextarea.value = JSON.stringify(JSON.parse(linkedTextarea.value), null, 4);
                } catch (ex) {
                    console.log(ex);
                }
            }
        }

        closeModal();
    }

    // BM Init to get preferences
    function init() {
        var isProduction = (window.location.href.toLowerCase().indexOf('production') > -1);
        var isStaging = (window.location.href.toLowerCase().indexOf('staging') > -1);
        var isGlobalCustomPreferences = (window.location.href.toLowerCase().indexOf('globalcustompreferences') > -1);
        var isSiteCustomPreferences = (window.location.hash && window.location.hash.length > 0 && (window.location.hash.toLowerCase().indexOf('site_preference_group') > -1 || window.location.hash.toLowerCase().indexOf('preference') > -1));
        var isAliases = (document.getElementsByName('saveAliases').length > 0);
        var isOCAPISettings = (window.location.href.toLowerCase().indexOf('viewwapisettings-start') > -1);
        var isDevelopmentSetup = (window.location.href.toLowerCase().indexOf('viewstudiosetup-start') > -1);

        // Load preferences
        chrome.storage.sync.get({
            enableKeepBMSessionAlive: false,
            enableOnStaging: false,
            enableValidateJson: false
        }, function (items) {
            if (!isProduction && ((isStaging && items.enableOnStaging) || (!isStaging && items.enableKeepBMSessionAlive))) {
                keepBMSessionAlive();
            }

            if (items.enableValidateJson && (isGlobalCustomPreferences || isSiteCustomPreferences || isAliases || isOCAPISettings)) {
                // Append modal form to document
                initModal();

                // Load Modal editors
                initModalEditor();

                // Start mutattion observer
                initMutationObersver();
            }
        });

        if (isDevelopmentSetup) {
            var allWebDavLinks = document.querySelectorAll('[aria-label*="/webdav/Sites/"]');
            if (allWebDavLinks.length) {
                allWebDavLinks.forEach( (elm) => {
                    console.log(elm.getAttribute('aria-label'));

                    var btnOpenLink = document.createElement('a');
                    btnOpenLink.setAttribute('href', elm.getAttribute('aria-label'));
                    btnOpenLink.setAttribute('target', '_blank');
                    btnOpenLink.setAttribute('class', 'webdav-list-item-link dw-nc-button');
                    btnOpenLink.setAttribute('style', 'margin-right: 15px;')
                    btnOpenLink.innerHTML = '<span class="fa fa-external-link"></span>';
                    
                    elm.before(btnOpenLink);

                    elm.parentElement.parentElement.setAttribute('style', 'width: 140px;')
                })
            }
        }
    }

    init();
})();