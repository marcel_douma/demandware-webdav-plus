
chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
    if (request && request.action === 'disableShelf') {
        // @todo: Figure out how to do properly. As if already disabled, this will throw an error PLUS need to retain the current user setting
        //chrome.downloads.setShelfEnabled(false);
    } else if (request && request.action === 'enableShelf') {
        //chrome.downloads.setShelfEnabled(true);
    } else {
        chrome.downloads.download({
            url: request.url,
            filename: 'DemandwareWEBDavPlus/' + request.domain + '/' + request.path + '/' + request.name,
            conflictAction: "overwrite"
        }, function () {
            sendResponse({
                farewell: "goodbye"
            });
        });    
    }

    return true;
});
