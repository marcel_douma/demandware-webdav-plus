'use strict';

function WebDavUpload() {
    var that = this;

    this._totalFilesAndDirectories = 0;
    this._totalUploaded = 0;
    this._totalFailed = 0;

    this.doFetch = function(method, isDirectory, entry, path) {
        let fetchURL = (isDirectory) ? window.location.href + path : window.location.href + path + '/' + entry.name;
        return fetch(fetchURL, {
                credentials: 'include',
                method: method,
                body: entry
            })
            .then(function(data) {
                if (isDirectory) {
                    if (data.status === 201 || data.status === 405) {
                        that._totalUploaded++;
                    }

                    return entry.getFilesAndDirectories().then(function(subFilesAndDirs) {
                        return that.iterateFilesAndDirectories(subFilesAndDirs, path);
                    });
                } else {
                    if (data.status === 201 || data.status === 204) {
                        that._totalUploaded++;
                    } else {
                        that._totalFailed++;
                    }

                    if (typeof that.updateUploadUI === "function") { 
                        that.updateUploadUI();
                    }

                    return true;
                }
            })
            .catch(function(error) {
                console.log('Request failed', error);
                this._totalFailed++;
            });
    };

    this.iterateFilesAndDirectories = function(filesAndDirs, path) {
        that._totalFilesAndDirectories += filesAndDirs.length;

        let allPromises = [],
            putOrPost = (window.location.href.toLowerCase().indexOf('webdav/sites/cartridges') > -1) ? 'PUT' : 'POST';


        for (var i = 0; i < filesAndDirs.length; i++) {
            allPromises.push(new Promise(function(resolve) {
                if (typeof filesAndDirs[i].getFilesAndDirectories === 'function') {
                    that.doFetch('MKCOL', true, filesAndDirs[i], filesAndDirs[i].path)
                    .then( () => resolve() );
                } else {
                    that.doFetch(putOrPost, false, filesAndDirs[i], path)
                    .then( () => resolve() );
                }
            }));
        }

        return Promise.all(allPromises);
    };
}

WebDavUpload.prototype.createDirectory = function(path) {
    var p = new Promise(function(resolve) {
        fetch(window.location.href + '/' + path, {
                credentials: 'include',
                method: 'MKCOL'
            })
            .then( () => resolve() )
            .catch(function(error) {
                console.log('Request failed', error);
            });
    });

    return p;
};

WebDavUpload.prototype.deleteFileOrDirectory = function(path) {
    var p = new Promise(function(resolve) {
        fetch(window.location.href + '/' + path, {
                credentials: 'include',
                method: 'DELETE',
                body: ''
            })
            .then( () => resolve() )
            .catch(function(error) {
                console.log('Request failed', error);
            });
    });

    return p;
};

WebDavUpload.prototype.uploadFilesAndDirectories = function(filesAndDirs, path) {
    var that = this;
    var p = new Promise(function(resolve) {
        that.iterateFilesAndDirectories(filesAndDirs, path)
        .then( () => resolve() );
    });

    return p;
};
