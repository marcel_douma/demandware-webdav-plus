'use strict';

function WebDavDownload() {
    this._totalDownloaded = 0;
    this._totalFiles = 0;
}

WebDavDownload.prototype.downloadFile = function(dataURI) {
    var that = this,
        p = new Promise(function(resolve) {
            let index = dataURI.lastIndexOf("/") + 1,
                baseFolder = dataURI.substring(dataURI.toUpperCase().indexOf('/WEBDAV/SITES/') + 14),
                filename = dataURI.substr(index),
                msg = {
                    name: filename,
                    path: baseFolder.replace('/' + filename, ''),
                    domain: window.location.hostname,
                    url: 'https://' + window.location.hostname + dataURI
                };

            that._totalFiles++;

            if (typeof that.updateDownloadUI === "function") {
                that.updateDownloadUI();
            }

            chrome.runtime.sendMessage(msg, function() {
                that._totalDownloaded++;

                if (typeof that.updateDownloadUI === "function") {
                    that.updateDownloadUI();
                }

                resolve();
            });
        });

    return p;
};

WebDavDownload.prototype.downloadDirectory = function(dataURI) {
    var that = this;

    chrome.runtime.sendMessage({action: 'disableShelf'});

    return fetch('https://' + window.location.hostname + dataURI, {
            credentials: 'include'
        })
        .then(function(data) {
            return data.text();
        })
        .then(function(data) {
            let allPromises = [];


            $(data).find('a tt').each(function(i, link) {
                allPromises.push(new Promise(function(resolve) {
                    let path = dataURI + '/' + $(link).text(),
                        index = path.lastIndexOf("/") + 1,
                        filename = path.substr(index);

                    if (filename.indexOf('.') > -1) {
                        that.downloadFile(path)
                            .then(() => resolve() );
                    } else {
                        that.downloadDirectory(path)
                            .then(() => resolve() );
                    }
                }));
            });

            return Promise.all(allPromises);
        })
        .then(function() {
            chrome.runtime.sendMessage({action: 'enableShelf'});
        })
        .catch(function(error) {
            console.log('Request failed', error);
        });
};