function WebDavUpload() {
    var that = this;

    this._totalFilesAndDirectories = 0;
    this._totalUploaded = 0;
    this._totalFailed = 0;

    this.doFetch = function(method, isDirectory, entry, path) {
        let fetchURL = (isDirectory) ? window.location.href + path : window.location.href + path + '/' + entry.name;
        return fetch(fetchURL, {
                credentials: 'include',
                method: method,
                body: entry
            })
            .then(function(data) {
                if (isDirectory) {
                    if (data.status === 201 || data.status === 405) {
                        that._totalUploaded++;
                    }

                    return entry.getFilesAndDirectories().then(function(subFilesAndDirs) {
                        return that.iterateFilesAndDirectories(subFilesAndDirs, path);
                    });
                } else {
                    if (data.status === 201 || data.status === 204) {
                        that._totalUploaded++;
                    } else {
                        that._totalFailed++;
                    }

                    if (typeof that.updateUploadUI === "function") { 
                        that.updateUploadUI();
                    }

                    return true;
                }
            })
            .catch(function(error) {
                console.log('Request failed', error);
                this._totalFailed++;
            });
    };

    this.iterateFilesAndDirectories = function(filesAndDirs, path) {
        that._totalFilesAndDirectories += filesAndDirs.length;

        let allPromises = [],
            putOrPost = (window.location.href.toLowerCase().indexOf('webdav/sites/cartridges') > -1) ? 'PUT' : 'POST';


        for (var i = 0; i < filesAndDirs.length; i++) {
            allPromises.push(new Promise(function(resolve) {
                if (typeof filesAndDirs[i].getFilesAndDirectories === 'function') {
                    that.doFetch('MKCOL', true, filesAndDirs[i], filesAndDirs[i].path)
                    .then( () => resolve() );
                } else {
                    that.doFetch(putOrPost, false, filesAndDirs[i], path)
                    .then( () => resolve() );
                }
            }));
        }

        return Promise.all(allPromises);
    };
}

WebDavUpload.prototype.createDirectory = function(path) {
    var p = new Promise(function(resolve) {
        fetch(window.location.href + '/' + path, {
                credentials: 'include',
                method: 'MKCOL'
            })
            .then( () => resolve() )
            .catch(function(error) {
                console.log('Request failed', error);
            });
    });

    return p;
};

WebDavUpload.prototype.deleteFileOrDirectory = function(path) {
    var p = new Promise(function(resolve) {
        fetch(window.location.href + '/' + path, {
                credentials: 'include',
                method: 'DELETE',
                body: ''
            })
            .then( () => resolve() )
            .catch(function(error) {
                console.log('Request failed', error);
            });
    });

    return p;
};

WebDavUpload.prototype.uploadFilesAndDirectories = function(filesAndDirs, path) {
    var that = this;
    var p = new Promise(function(resolve) {
        that.iterateFilesAndDirectories(filesAndDirs, path)
        .then( () => resolve() );
    });

    return p;
};

WebDavUpload.prototype.updateUploadUI = function() {
    let totalProgress = ((this._totalUploaded + this._totalFailed) / this._totalFilesAndDirectories * 100).toFixed(0);

    $('#uploadProgressBar .progress-bar').width(totalProgress + '%');
    $('#uploadProgressBar .progress-bar').html(totalProgress + '%');
};

(function() {
    function init() {
        var preferences = JSON.parse($('body').attr('data-preferences'));
        if( ( preferences.enableOnProduction || !preferences.isProduction) || (preferences.isLogView && preferences.enableOnLogview) ) {
            //add drop events
            window.addEventListener("dragover", function(e) {
                e = e || event;
                e.preventDefault();
        
                $('#dropZone').show();
            }, false);
        
            window.addEventListener("drop", function(e) {
                e = e || event;
                e.stopPropagation();
                e.preventDefault();
        
                $('#dropZone').hide();
                $('#uploadProgressBar').show();
                $(".btn").attr("disabled", "disabled");
                $(".checkbox-file").attr("disabled", "disabled");
                $('body').on('click', 'a', function(event) {
                    event.preventDefault();
                    console.log('links and buttons are disabled while uploading.');
                });
        
        
                // begin by traversing the chosen files and directories
                if ('getFilesAndDirectories' in e.dataTransfer) {
                    e.dataTransfer.getFilesAndDirectories().then(function(filesAndDirs) {
                        let webDav = new WebDavUpload();
        
                        webDav.uploadFilesAndDirectories(filesAndDirs, '/')
                            .then(() => {
                                //2.5 seconds to shine!
                                setTimeout(function() {
                                    location.reload();
                                }, 2500);
                            });
                    });
                }
            }, false);
        
        
            $("#btnUpload").on("click", function() {
                $('#dropZone').toggle();
            });
        
            $("#btnCreateDirectory").on("click", function() {
                let directoryName = prompt("Please enter the name of the directory to create", "");
                if (directoryName !== null && directoryName.length > 0) {
        
                    let webDav = new WebDavUpload();
                    webDav.createDirectory('/' + directoryName).then(function() {
                        location.reload();
                    });
                }
            }); 
        
            $('#btnDelete').on('click', function() {
                let msgConfirm = "Are you sure you want to delete the selected file(s) and / or directory(s)?";
        
                if (confirm(msgConfirm)) {
                    let allPromises = [];
        
                    $('.checkbox-file').each(function() {
                        if ($(this).closest('tr').is(":visible")) {
                            let index = $(this).attr("data-uri").lastIndexOf("/") + 1,
                                filename = $(this).attr("data-uri").substr(index);
        
                            if (this.checked) {
                                allPromises.push(new Promise(function(resolve) {
                                    let webDav = new WebDavUpload();
                                    webDav.deleteFileOrDirectory(filename).then(function() {
                                        resolve();
                                    });
                                }));
                            }
                        }
                    });
        
                    Promise.all(allPromises).then(() => location.reload());
                }
            });           
        }
    }

    init();
})();
