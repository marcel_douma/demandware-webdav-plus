'use strict';

function WebDavView() {
    this.initToolbar = function() {
        var filterTable = function( keyword ) {
            var data = keyword.toUpperCase().split(" ");

            //create a jquery object of the rows
            var jo = $("#tblWebDavPlus").find("tbody tr");
            if (keyword == "") {
                jo.show();
                return;
            }
            //hide all the rows
            jo.hide();

            //Recusively filter the jquery object to get results.
            jo.filter(function (i, v) {
                var $t = $(this);
                for (var d = 0; d < data.length; ++d) {
                    if ($t.text().toUpperCase().indexOf(data[d]) > -1) {
                        return true;
                    }
                }
                return false;
            })
            //show the rows that match.
            .show();
        },
        btnUploadClass = (( this.preferences.enableOnProduction || !this.preferences.isProduction) ? '' : ' disabled" title="Disabled, go to extension options to enable this." disabled="disabled');


        if( this.preferences.isLogView && !this.preferences.enableOnLogview ) {
            btnUploadClass = ' hide';
        }
        
        //add toolbar buttons and create directory button
        $(`<form method="post" id="myForm" class="form-horizontal">
            <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                <div class="btn-group mr-2" role="group">
                    <button type="button" class="btn btn-light" id="btnToggleSelection">Toggle Selection</button>
                    <button type="button" class="btn btn-light" id="btnParentDirectory">Parent directory</button>
                </div>

                <div class="btn-group mr-2" role="group">
                    <button type="button" class="btn btn-primary` + btnUploadClass + `" id="btnUpload">Upload</button>
                    <button type="button" class="btn btn-primary" id="btnDownload">Download</button>
                    <button type="button" class="btn btn-light` + btnUploadClass + `" id="btnCreateDirectory">Create directory</button>
                </div>

                <div class="btn-group mr-2" role="group">
                    <button type="button" class="btn btn-danger` + btnUploadClass + `" id="btnDelete">Delete selection</button>
                </div>
            </div>
            <div class="form-group mt-3">
                <input id="searchInput" placeholder="Type To Filter" class="form-control">
            </div>

            <div class="progress" id="uploadProgressBar">
                <div class="progress-bar" role="progressbar" style="min-width: 2em;width: 0%;">0%</div>
            </div>
            <div id="dropZone" class="well">Drag & drop your files and/or directories here!</div>
        </form>`).insertAfter("h1:first");


        //set button events
        $("#searchInput").keyup(function() {
            filterTable(this.value);
        }).focus(function() {
            this.value = "";
            $(this).unbind('focus');
        });

        $('#btnParentDirectory').on("click", function() {
            window.location.replace($('#btnParentDirectory').attr('data-uri'));
        });

        $("#btnToggleSelection").on("click", function() {
            $('.checkbox-file').each(function() {
                if ($(this).closest('tr').is(":visible")) {
                    this.checked = !this.checked;
                }
            });
        });


        $("#btnDownload").on("click", function() {
            let allPromises = [],
                webDav = new WebDavDownload();

            $('#uploadProgressBar').show();
            $(".btn").prop("disabled", true);
            $(".checkbox-file").prop("disabled", true);
            $('body').on('click', 'a', function(event) {
                event.preventDefault();
                console.log('links and buttons are disabled while uploading.');
            });

            $('.checkbox-file').each(function() {
                let $this = $(this);                    

                if (this.checked && $this.closest('tr').is(":visible")) {
                    allPromises.push(new Promise(function(resolve) {
                        let isFile = $this.hasClass("is-file"),
                            isDirectory = $this.hasClass("is-directory"),
                            dataURI = $this.attr("data-uri");

                        if (isFile) {
                            webDav.downloadFile(dataURI)
                                .then(() => resolve() );
                        } else if (isDirectory) {
                            webDav.downloadDirectory(dataURI)
                                .then(() => resolve() );
                        }
                    }));
                }
            });

            Promise.all(allPromises).then(() => {
                //2.5 seconds to shine!
                setTimeout(function() {
                    //deselect all checkboxes
                    $('.checkbox-file').each(function() {
                        if ($(this).closest('tr').is(":visible")) {
                            this.checked = false;
                        }
                    });

                    //enable all buttons and links            
                    $('#uploadProgressBar').hide();
                    $(".btn").prop("disabled", false);
                    $(".checkbox-file").prop("disabled", false);
                    $('body').on('click', 'a', function(event) {
                    });

                    //reset progressbar
                    if (typeof webDav.resetProgressBar === "function") {
                        webDav.resetProgressBar();
                    }
                }, 2500);
            });
        });

        //set a default filter on error files when on webdav log directory
        
        if ( this.preferences.isLogView ) {
            var defaultFilterKeyword = this.preferences.defaultFilterKeyword;

            if( this.preferences.defaultFilterOnToday ) {
                defaultFilterKeyword = moment().format("YYYYMMDD");
            }

            if( defaultFilterKeyword.length > 0 ) {
                filterTable(defaultFilterKeyword);

                $("#searchInput").val(defaultFilterKeyword);                
            }
        }
        
    };

    this.init = function(preferences) {
        this.preferences = preferences;

        $('body').attr('data-preferences', JSON.stringify(preferences));

        $('hr').remove();

        var tblWebDavPlus = $("table:first");
        var parentDirectoryURL = window.location.href;

        if (tblWebDavPlus.length > 0) {
            tblWebDavPlus.attr('id', 'tblWebDavPlus');
            tblWebDavPlus.addClass("tablesorter table table-sm table-striped table-hover");

            var thead = tblWebDavPlus.find("thead");
            var thRows = tblWebDavPlus.find("tr:first");

            if (thead.length === 0) { //if there is no thead element, add one.
                thead = $("<thead></thead>").appendTo(tblWebDavPlus);
            }

            thead.append(thRows);
            //tblWebDavPlus.find("tr:first").remove();


            $('#tblWebDavPlus a').each(function() {
                if ($(this).text().indexOf(".log") > 0 || $(this).text().indexOf(".xml") > 0) {
                    $(this).attr("target", "_blank");
                }

                //create a checkbox button, skip Parent Directory
                if ($(this).html().indexOf("<tt>") > -1) {
                    var dataURI = $(this).attr("href");

                    var index = dataURI.lastIndexOf("/") + 1;
                    var filename = dataURI.substr(index);
                    var className = "";

                    //assume it is a file if there is a dot in the filename
                    if (filename.indexOf('.') > -1) {
                        className += "is-file";
                    } else {
                        className += "is-directory";
                    }

                    $(this).after('<input type="checkbox" class="checkbox-file ' + className + ' float-left mt-1" data-uri="' + $(this).attr("href") + '" />');
                    $(this).addClass(className);
                } else {
                    $(this).addClass("is-parent-directory");
                    parentDirectoryURL = $(this).attr("href");
                }
            });

            //load toolbar with all buttons
            this.initToolbar();

            //move parent directory link to button
            $('.is-parent-directory').closest('tr').remove();
            $('#btnParentDirectory').attr('data-uri', parentDirectoryURL);

            // First, load jQuery dependecy
            ["js/vendor/jquery.min.js"].forEach( function(jsScript) {
                // Create a new script element
                var script = window.document.createElement( 'script' );

                // Set the script element `src`
                script.src = chrome.extension.getURL(jsScript);

                // Inject the script into the DOM
                document.head.appendChild(script);
            });

            // Secondly, (nasty hack), wait 1 seconds for jQuery to be loaded
            setTimeout( function() {
                ["js/polyfill.js","js/webdav/WebDavPlus.min.js"].forEach( function(jsScript) {
                    // Create a new script element
                    var script = window.document.createElement( 'script' );

                    // Set the script element `src`
                    script.src = chrome.extension.getURL(jsScript);

                    // Inject the script into the DOM
                    document.head.appendChild(script);
                });
            }, 500);

            //enable table sorting
            $('<script>$(function(){  enableSorting() });</script>').appendTo('body');


        }
    };


}
