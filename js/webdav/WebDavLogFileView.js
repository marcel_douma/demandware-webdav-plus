'use strict';

function WebDavLogFileView() {
    this.groupBy = function(array, f) {
        var groups = {};
        array.forEach(function(o) {
            var group = JSON.stringify(f(o));
            groups[group] = groups[group] || [];
            groups[group].push(o);
        });

        return Object.keys(groups).map(function(group) {
            return groups[group];
        });
    };

    this.initGroupedLog = function() {
        var url = window.location.href;
        var index = url.lastIndexOf("/") + 1;
        var filename = url.substr(index);

        $('body').prepend('<h1>' + filename + '</h1>');
        $('body pre').attr("style", "");

        var allDates = $('body pre').text().match(/\[\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}.\d{3}\sGMT\]/g);
        var allText = $('body pre').text().split(/\[\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}.\d{3}\sGMT\]/g);

        var logLines = [];
        var idx = 0;

        allDates.forEach(function(logDate) {
            idx++;

            var logText = allText[idx].trim();
            var logType = logText.substr(0, logText.indexOf(' '));

            logText = logText.substr(logType.length + 1).trim();

            var logDetails = logText.split('|');

            logLines.push({
                "logDate": logDate.replace("GMT]", "").replace("[", "").trim().substring(0, 24).trim(),
                "text": logDetails.length > 2 ? logDetails[3] : logDetails[0],
                "errorType": logType,
                "site": logDetails[2]
            });
        });

        var groupedResult = this.groupBy(logLines, function(item) {
            return [item.site, item.text];
        });


        $('body pre').html(`<table id='myLogTable' class='tablesorter table logtable' cellpadding='10'>
            <thead>
            <tr>
                <th width='50'>Count</th>
                <th width='200'>Site</th>
                <th>Description</th>
            </tr>
            </thead>
            <tbody></tbody>
            </table>`);

        groupedResult.forEach(function(result) {
            var firstResult = result[0];

            if (firstResult !== null && firstResult.text !== null) {
                //retain line breaks
                firstResult.text = firstResult.text.replace(/\n/g, "<br />");

                //add tags
                firstResult.text = firstResult.text.replace(/(System Information)/g, "<br/><br/><span class=\"label label-default loglabel\">system information:</span><br/>");
                firstResult.text = firstResult.text.replace(/(Request Information)/g, "<br/><br/><span class=\"label label-default loglabel\">request information:</span><br/>");
                firstResult.text = firstResult.text.replace(/(Request Parameters)/g, "<br/><br/><span class=\"label label-default loglabel\">request parameters:</span><br/>");

                $('<tr><td>' + result.length + '</td><td>' + firstResult.site + '</td><td>' + firstResult.text + '</td></tr>').appendTo('#myLogTable tbody');
            }
        });
    };

    this.initLog = function() {
        var url = window.location.href;
        var index = url.lastIndexOf("/") + 1;
        var filename = url.substr(index);

        $('body').prepend('<h1>' + filename + '</h1><p>Note: This list is limited to the last 50 messages.</p><a class="btn btn-light" href="' + url + '?grouplog">Group log</a><hr class="mb-4">');
        $('body pre').attr("style", "");

        //var regex = new RegExp('\[\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}.\d{3}\s\D{3}\]', 'g');

        var re = new RegExp('GMT]', 'g');

        //get body text and keep line breaks
        var logBody = $('body pre').text().replace(/\n/g, "<br />");
        var matches = logBody.match(re);
        var idxStart = 0;
        var idxMatch = 0;
        var m;

        //skip until X left
        if (matches.length > 50) {
            idxStart = (matches.length - 50);
        }


        while (m = re.exec(logBody)) {
            idxMatch++;

            if (idxMatch > idxStart) {
                var logDate = logBody.slice((m.index - 25), (m.index + 4)),
                    newLogDate = logDate.replace("GMT]", "").replace("[", "").trim().substring(0, 24).trim();

                logBody = logBody.replace(logDate, "</td></tr><tr valign=\"top\"><td>" + newLogDate + "</td><td>");
            }
        }

        //add tags
        logBody = logBody.replace(/(System Information)/g, "<br/><br/><span class=\"label label-default loglabel\">system information:</span><br/>");
        logBody = logBody.replace(/(Request Information)/g, "<br/><br/><span class=\"label label-default loglabel\">request information:</span><br/>");
        logBody = logBody.replace(/(Request Parameters)/g, "<br/><br/><span class=\"label label-default loglabel\">request parameters:</span><br/>");

        $('body pre').html("<table id='myLogTable' class='tablesorter table logtable' cellpadding='10'><thead><tr><th width='240'>Date</th><th>Description</th></tr></thead><tbody><tr><td>" + logBody + "</td></tr></tbody></table>");

        $('#myLogTable tbody tr:first').remove();

        $('<script>$(function(){  enableLogSorting() });</script>').appendTo('body');
    };

    this.init = function() {
        if (window.location.href.indexOf("grouplog") > 0) {
            this.initGroupedLog();
        } else {
            this.initLog();
        }
    };
}

String.prototype.insert = function(index, string) {
    if (index > 0)
        return this.substring(0, index) + string + this.substring(index, this.length);
    else
        return string + this;
};
