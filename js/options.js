// Saves options to chrome.storage
function save_options() {
    var enableOnProduction = document.getElementById('enableOnProduction').checked;
    var enableOnLogview = document.getElementById('enableOnLogview').checked;
    var defaultFilterKeyword = document.getElementById('defaultFilterKeyword').value;
    var defaultFilterOnToday = document.getElementById('defaultFilterOnToday').checked;
    var enableKeepBMSessionAlive = document.getElementById('enableKeepBMSessionAlive').checked;
    var enableOnStaging = document.getElementById('enableOnStaging').checked;
    var enableValidateJson = document.getElementById('enableValidateJson').checked;
    
    chrome.storage.sync.set({
        enableOnProduction: enableOnProduction,
        enableOnLogview: enableOnLogview,
        defaultFilterKeyword: defaultFilterKeyword,
        defaultFilterOnToday: defaultFilterOnToday,
        enableKeepBMSessionAlive: enableKeepBMSessionAlive,
        enableOnStaging: enableOnStaging,
        enableValidateJson: enableValidateJson
    }, function () {
        // Update status to let user know options were saved.
        var status = document.getElementById('status');
        status.textContent = 'Options saved.';
        status.className = 'alert alert-success';
        setTimeout(function () {
            status.textContent = '';
            status.className = 'hide';
        }, 1250);
    });
}

// Restores select box and checkbox state using the preferences
// stored in chrome.storage.
function restore_options() {
    chrome.storage.sync.get({
        enableOnProduction: false,
        enableOnLogview: false,
        defaultFilterKeyword: '',
        defaultFilterOnToday: false,
        enableKeepBMSessionAlive: false,
        enableOnStaging: false,
        enableValidateJson: false
    }, function (items) {
        document.getElementById('enableOnProduction').checked = items.enableOnProduction;
        document.getElementById('enableOnLogview').checked = items.enableOnLogview;
        document.getElementById('defaultFilterKeyword').value = items.defaultFilterKeyword;
        document.getElementById('defaultFilterOnToday').checked = items.defaultFilterOnToday;
        document.getElementById('enableKeepBMSessionAlive').checked = items.enableKeepBMSessionAlive;
        document.getElementById('enableOnStaging').checked = items.enableOnStaging;
        document.getElementById('enableValidateJson').checked = items.enableValidateJson;
    });
}
document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('save').addEventListener('click', save_options);