'use strict';

(function() {
    WebDavDownload.prototype.updateDownloadUI = function() {
        let totalProgress = (this._totalDownloaded / this._totalFiles * 100).toFixed(0);

        $('#uploadProgressBar .progress-bar').width(totalProgress + '%');
        $('#uploadProgressBar .progress-bar').html(totalProgress + '%');
    };

    WebDavDownload.prototype.resetProgressBar = function() {
        $('#uploadProgressBar .progress-bar').width('0%');
        $('#uploadProgressBar .progress-bar').html('0%');
    }

    function init() {
        var webdav,
            preferences = {
                isProduction: (window.location.href.toLowerCase().indexOf("production") > -1),
                isLogFile: (window.location.href.indexOf(".log") > 0),
                isLogView: (window.location.href.toUpperCase().indexOf('/WEBDAV/SITES/LOGS') > -1),
                enableOnProduction: false,
                enableOnLogview: false,
                defaultFilterKeyword: '',
                defaultFilterOnToday: false
            };

        //load preferences
        chrome.storage.sync.get({
            enableOnProduction: false,
            enableOnLogview: false,
            defaultFilterKeyword: '',
            defaultFilterOnToday: false
        }, function(items) {
            preferences.enableOnProduction = items.enableOnProduction; //(items.enableOnProduction && window.location.href.toLowerCase().indexOf("production") > -1);
            preferences.enableOnLogview = items.enableOnLogview;
            preferences.defaultFilterKeyword = items.defaultFilterKeyword;
            preferences.defaultFilterOnToday = items.defaultFilterOnToday;

            if (window.location.href.indexOf("/on/demandware.servlet/webdav/") > -1) {
                if (window.location.href.indexOf(".log") > 0) {
                    webdav = new WebDavLogFileView();
                } else {
                    webdav = new WebDavView();
                }

                webdav.init(preferences);
            }
        });
    }

    init();
})();

/*

These function are using the jQuery table sorter script

*/

function enableLogSorting() {
    $.tablesorter.addParser({
        id: 'customDate',
        is: function() {
            return false;
        }, // don't auto detect this parser
        format: function(s) {
            return moment(s, "YYYY-MM-DD HH:mm:ss.SSS").utc();
        },
        type: 'numeric'
    });

    $("#myLogTable").tablesorter({
        headers: {
            0: { sorter: 'customDate' }
        },
        sortList: [
            [0, 1]
        ]
    });
}

function enableSorting() {
    $.tablesorter.addParser({
        id: 'customDate',
        is: function() {
            return false;
        }, // don't auto detect this parser
        format: function(s) {
            //Fri, 06 Mar 2015 08:13:56 GMT
            return moment(s.substring(4, 25), "DD MMM YYYY HH:mm:ss").utc();
        },
        type: 'numeric'
    });

    $("#tblWebDavPlus").tablesorter({
        headers: {
            2: { sorter: 'customDate' }
        },
        sortList: [
            [2, 1]
        ]
    });
}
